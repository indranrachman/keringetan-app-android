package com.indra.keringetan.login;

import com.google.firebase.auth.AuthResult;
import com.indra.keringetan.base.BaseView;

public interface LoginView extends BaseView {
    void googleSignInSuccess(AuthResult authResult);

    void googleSignInFailed(String message);

}
