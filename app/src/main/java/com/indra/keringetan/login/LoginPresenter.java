package com.indra.keringetan.login;

import android.support.annotation.NonNull;

import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.GoogleAuthProvider;
import com.indra.keringetan.base.BasePresenter;
import com.indra.keringetan.utils.NetworkChecker;

public class LoginPresenter extends BasePresenter<LoginView> {

    private FirebaseAuth firebaseAuth;

    public LoginPresenter(LoginView view, NetworkChecker networkChecker, FirebaseAuth firebaseAuth) {
        super(view, networkChecker);
        this.firebaseAuth = firebaseAuth;
    }

    public void firebaseAuthWithGoogle(GoogleSignInAccount account) {
        view.showLoading();
        AuthCredential credential = GoogleAuthProvider.getCredential(account.getIdToken(), null);
        firebaseAuth.signInWithCredential(credential)
                .addOnSuccessListener(new OnSuccessListener<AuthResult>() {
                    @Override
                    public void onSuccess(AuthResult authResult) {
                        view.removeLoading();
                        view.googleSignInSuccess(authResult);
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        view.removeLoading();
                        view.googleSignInFailed(e.getMessage());
                    }
                });
    }
}
