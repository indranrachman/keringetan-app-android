package com.indra.keringetan.login;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.View;

import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.firebase.auth.AuthResult;
import com.indra.keringetan.R;
import com.indra.keringetan.base.LoginBaseActivity;
import com.indra.keringetan.databinding.ActivityLoginBinding;
import com.indra.keringetan.main.MainActivity;

public class LoginActivity extends LoginBaseActivity implements GoogleApiClient.OnConnectionFailedListener, LoginView, View.OnClickListener {

    private static final int RC_SIGN_IN = 9001;

    private ActivityLoginBinding binding;
    private LoginPresenter presenter;
    private GoogleApiClient googleApiClient;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getString(R.string.default_web_client_id))
                .requestEmail()
                .build();

        googleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this, this)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();
        presenter = new LoginPresenter(this, getNetworkChecker(), getFirebaseAuth());
        binding = DataBindingUtil.setContentView(this, R.layout.activity_login);
        binding.signInButton.setOnClickListener(this);
    }

    @Override
    public void showLoading() {
        showProgressDialog();
    }

    @Override
    public void removeLoading() {
        dismissProgressDialog();
    }

    @Override
    public void noInternetConnection() {
        showSnackBar(binding.getRoot(), getString(R.string.no_internet_connection));
    }

    @Override
    public void googleSignInSuccess(AuthResult authResult) {
        startActivity(new Intent(this, MainActivity.class));
        finish();
    }

    @Override
    public void googleSignInFailed(String message) {
        showSnackBar(binding.getRoot(), message);
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        showSnackBar(binding.getRoot(), connectionResult.getErrorMessage());
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RC_SIGN_IN) {
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            if (result.isSuccess()) {
                GoogleSignInAccount account = result.getSignInAccount();
                presenter.firebaseAuthWithGoogle(account);
            } else {
                showSnackBar(binding.getRoot(), String.valueOf(result.getStatus()));
            }
        }
    }

    @Override
    public void onClick(View view) {
        Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(googleApiClient);
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }
}
