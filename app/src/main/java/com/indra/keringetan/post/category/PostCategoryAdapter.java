package com.indra.keringetan.post.category;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.indra.keringetan.R;
import com.indra.keringetan.databinding.ItemPostCategoryBinding;
import com.indra.keringetan.model.PostCategory;

import java.util.List;

public class PostCategoryAdapter extends PagerAdapter {

    private List<PostCategory> postCategories;
    private Context context;

    public PostCategoryAdapter(List<PostCategory> postCategories, Context context) {
        this.postCategories = postCategories;
        this.context = context;
    }

    @Override
    public int getCount() {
        return postCategories.size();
    }

    @Override
    public int getItemPosition(final Object object) {
        return POSITION_NONE;
    }

    @Override
    public Object instantiateItem(final ViewGroup container, final int position) {
        PostCategory postCategory = postCategories.get(position);
        ItemPostCategoryBinding binding = DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.item_post_category, container, false);
        binding.imgItem.setImageResource(postCategory.drawableResource);
        binding.txtItem.setText(postCategory.title);
        container.addView(binding.getRoot());
        return binding.getRoot();
    }

    @Override
    public boolean isViewFromObject(final View view, final Object object) {
        return view.equals(object);
    }

    @Override
    public void destroyItem(final ViewGroup container, final int position, final Object object) {
        container.removeView((View) object);
    }
}
