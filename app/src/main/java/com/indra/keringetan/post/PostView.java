package com.indra.keringetan.post;

import com.indra.keringetan.base.BaseView;
import com.indra.keringetan.model.Post;

public interface PostView extends BaseView {
    Post getUserPostRequest();

    void sendUserPostFailed(String message);

    void sendUserPostSuccess();

    void openLocationPickerScreen();

}
