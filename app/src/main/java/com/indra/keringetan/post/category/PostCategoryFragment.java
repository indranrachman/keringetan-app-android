package com.indra.keringetan.post.category;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;

import com.gigamole.infinitecycleviewpager.OnInfiniteCyclePageTransformListener;
import com.indra.keringetan.R;
import com.indra.keringetan.databinding.FragmentPostCategoryBinding;
import com.indra.keringetan.model.PostCategory;
import com.indra.keringetan.post.PostActivity;
import com.indra.keringetan.utils.GeneralHelper;

import java.util.List;

public class PostCategoryFragment extends Fragment implements OnInfiniteCyclePageTransformListener {

    private PostCategoryView postCategoryView;
    private FragmentPostCategoryBinding binding;
    private List<PostCategory> postCategories;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        if (context instanceof PostActivity) {
            try {
                postCategoryView = (PostCategoryView) context;
            } catch (ClassCastException e) {
                throw new ClassCastException(context.toString()
                        + " must implement PostCategoryView");
            }
        }
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        postCategories = GeneralHelper.getSportCategoryList();
    }

    @Nullable
    @Override
    public View onCreateView(final LayoutInflater inflater, @Nullable final ViewGroup container, @Nullable final Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_post_category, container, false);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(final View view, @Nullable final Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        binding.pagerPostCategory.setAdapter(new PostCategoryAdapter(postCategories, getContext()));

        binding.pagerPostCategory.setScrollDuration(400);
        binding.pagerPostCategory.setCurrentItem(binding.pagerPostCategory.getRealItem());
        binding.pagerPostCategory.setOnInfiniteCyclePageTransformListener(this);
    }

    @Override
    public void onPreTransform(View page, float position) {

        postCategoryView.getCurrentItem(binding.pagerPostCategory.getRealItem());
    }

    @Override
    public void onPostTransform(View page, float position) {
        postCategoryView.getCurrentItem(binding.pagerPostCategory.getRealItem());
    }
}
