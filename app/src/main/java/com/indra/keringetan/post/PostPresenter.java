package com.indra.keringetan.post;

import android.support.annotation.NonNull;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DatabaseReference;
import com.indra.keringetan.base.BasePresenter;
import com.indra.keringetan.utils.NetworkChecker;

public class PostPresenter extends BasePresenter<PostView> {

    private DatabaseReference databaseReference;

    public PostPresenter(PostView view, NetworkChecker networkChecker, DatabaseReference databaseReference) {
        super(view, networkChecker);
        this.databaseReference = databaseReference;
    }

    public void onSetLocationClicked() {
        view.openLocationPickerScreen();
    }

    public void onPostButtonClicked() {
        view.showLoading();
        databaseReference.child(databaseReference.push().getKey()).setValue(view.getUserPostRequest())
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        view.removeLoading();
                        view.sendUserPostSuccess();
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        view.removeLoading();
                        view.sendUserPostFailed(e.getMessage());
                    }
                });
    }
}
