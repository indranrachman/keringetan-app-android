package com.indra.keringetan.post;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import com.indra.keringetan.AppConstant;
import com.indra.keringetan.R;
import com.indra.keringetan.base.BaseActivity;
import com.indra.keringetan.databinding.ActivityPostBinding;
import com.indra.keringetan.model.Post;
import com.indra.keringetan.post.category.PostCategoryView;
import com.schibstedspain.leku.LocationPickerActivity;

public class PostActivity extends BaseActivity implements PostView, PostCategoryView {

    private ActivityPostBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_post);
        binding.setPresenter(new PostPresenter(this, getNetworkChecker(), getPostDatabaseReference()));
    }

    @Override
    public void showLoading() {
        binding.progressBar.setVisibility(View.VISIBLE);
        binding.buttonPost.setVisibility(View.GONE);
    }

    @Override
    public void removeLoading() {
        binding.progressBar.setVisibility(View.GONE);
        binding.buttonPost.setVisibility(View.VISIBLE);
    }

    @Override
    public void noInternetConnection() {
        showSnackBar(binding.getRoot(), getString(R.string.no_internet_connection));
    }

    @Override
    public Post getUserPostRequest() {
        return new Post(getFirebaseUser().getDisplayName(),
                binding.textDescription.getText().toString(),
                binding.textLocation.getText().toString(),
                String.valueOf(System.currentTimeMillis()),
                String.valueOf(getFirebaseUser().getPhotoUrl()));
    }

    @Override
    public void sendUserPostFailed(String message) {
        showSnackBar(binding.getRoot(), message);
        hideSoftKeyboard();
    }

    @Override
    public void sendUserPostSuccess() {
        showSnackBar(binding.getRoot(), getString(R.string.sucess_post_data));
        hideSoftKeyboard();
    }

    @Override
    public void openLocationPickerScreen() {
        Intent locationPickerIntent = new Intent(PostActivity.this, LocationPickerActivity.class);
        startActivityForResult(locationPickerIntent, AppConstant.LOCATION_PICKER_REQUEST);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == AppConstant.LOCATION_PICKER_REQUEST && resultCode == RESULT_OK) {
            String address = data.getStringExtra(LocationPickerActivity.LOCATION_ADDRESS);
            binding.textLocation.setText(address);
        }
        binding.textLocation.clearFocus();
    }

    @Override
    public void getCurrentItem(float position) {
        Log.e("activity", "getCurrentItem: " + position);
    }
}
