package com.indra.keringetan.post.category;

import android.support.v7.widget.CardView;

public interface PostCategoryView {
    public void getCurrentItem(float position);
}
