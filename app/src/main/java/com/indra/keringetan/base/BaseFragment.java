package com.indra.keringetan.base;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.indra.keringetan.KeringetanApp;
import com.indra.keringetan.R;
import com.indra.keringetan.utils.NetworkChecker;

import javax.inject.Inject;

public class BaseFragment extends Fragment {

    @Inject
    FirebaseAuth firebaseAuth;

    @Inject
    NetworkChecker networkChecker;

    @Inject
    DatabaseReference postDatabaseReference;

    private ProgressDialog dialog;
    private Context context;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ((KeringetanApp) getActivity().getApplication()).getAppComponent().inject(this);
    }

    @Override
    public void onStart() {
        super.onStart();
        if (firebaseAuth.getCurrentUser() != null) {
            firebaseAuth.getCurrentUser().reload();
        }
    }

    @Override
    public Context getContext() {
        return context;
    }

    public FirebaseAuth getFirebaseAuth() {
        return firebaseAuth;
    }

    public NetworkChecker getNetworkChecker() {
        return networkChecker;
    }

    public DatabaseReference getPostDatabaseReference() {
        return postDatabaseReference;
    }

    public FirebaseUser getFirebaseUser() {
        return firebaseAuth.getCurrentUser();
    }

    public void showProgressDialog() {
        dialog = new ProgressDialog(getActivity());
        dialog.setMessage(getString(R.string.loading));
        if (dialog != null && !dialog.isShowing()) {
            dialog.show();
        }
    }

    public void dismissProgressDialog() {
        if (dialog != null) {
            dialog.dismiss();
        }
    }

    public void showSnackBar(View view, String message) {
        if (context != null) {
            Snackbar snackbar = Snackbar.make(view, message, Snackbar.LENGTH_LONG);
            snackbar.getView().setBackgroundColor(ContextCompat.getColor(context, R.color.colorPrimaryDark));
            snackbar.show();
        }
    }

    public void hideSoftKeyboard() {
        InputMethodManager inputMethodManager = (InputMethodManager) context.getSystemService(Activity.INPUT_METHOD_SERVICE);
        View view = getActivity().getCurrentFocus();
        if (view == null) {
            view = new View(context);
        }
        inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }
}
