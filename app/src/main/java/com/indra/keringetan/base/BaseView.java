package com.indra.keringetan.base;

public interface BaseView {
    void showLoading();

    void removeLoading();

    void noInternetConnection();


}
