package com.indra.keringetan.base;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.indra.keringetan.KeringetanApp;
import com.indra.keringetan.R;
import com.indra.keringetan.utils.NetworkChecker;

import javax.inject.Inject;

public class BaseActivity extends AppCompatActivity {

    @Inject
    FirebaseAuth firebaseAuth;

    @Inject
    NetworkChecker networkChecker;

    @Inject
    DatabaseReference postDatabaseReference;

    private ProgressDialog dialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ((KeringetanApp) getApplication()).getAppComponent().inject(this);
    }

    @Override
    protected void onStart() {
        super.onStart();
        if (firebaseAuth.getCurrentUser() != null) {
            firebaseAuth.getCurrentUser().reload();
        }
    }

    public FirebaseAuth getFirebaseAuth() {
        return firebaseAuth;
    }

    public NetworkChecker getNetworkChecker() {
        return networkChecker;
    }

    public DatabaseReference getPostDatabaseReference() {
        return postDatabaseReference;
    }

    public FirebaseUser getFirebaseUser() {
        return firebaseAuth.getCurrentUser();
    }

    public void showProgressDialog() {
        dialog = new ProgressDialog(this);
        dialog.setMessage(getString(R.string.loading));
        if (dialog != null && !dialog.isShowing()) {
            dialog.show();
        }
    }

    public void dismissProgressDialog() {
        if (dialog != null) {
            dialog.dismiss();
        }
    }

    public void showSnackBar(View view, String message) {
        Snackbar snackbar = Snackbar.make(view, message, Snackbar.LENGTH_LONG);
        snackbar.getView().setBackgroundColor(ContextCompat.getColor(this, R.color.test));
        snackbar.show();
    }

    public void hideSoftKeyboard() {
        InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(Activity.INPUT_METHOD_SERVICE);
        View view = getCurrentFocus();
        if (view == null) {
            view = new View(this);
        }
        inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    protected void changeFragment(Fragment fragment, String tag) {
        if (!isFinishing()) {
            getSupportFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
            FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
            fragmentTransaction.setTransition(FragmentTransaction.TRANSIT_NONE);
            fragmentTransaction.replace(R.id.layout_container, fragment, tag);
            fragmentTransaction.commit();
        }
    }
}
