package com.indra.keringetan.base;

import com.indra.keringetan.utils.NetworkChecker;

import rx.subscriptions.CompositeSubscription;

public abstract class BasePresenter<T extends BaseView> {

    protected NetworkChecker networkChecker;
    protected T view;

    public BasePresenter(T view, NetworkChecker networkChecker) {
        this.view = view;
        this.networkChecker = networkChecker;
    }

    protected boolean connectedToNetwork() {
        if (!networkChecker.isConnected()) {
            view.removeLoading();
            view.noInternetConnection();
            return false;
        }
        return true;
    }

}
