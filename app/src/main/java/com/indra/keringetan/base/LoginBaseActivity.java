package com.indra.keringetan.base;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.google.firebase.auth.FirebaseAuth;
import com.indra.keringetan.KeringetanApp;
import com.indra.keringetan.R;
import com.indra.keringetan.utils.NetworkChecker;

import javax.inject.Inject;

public class LoginBaseActivity extends AppCompatActivity {

    @Inject
    FirebaseAuth firebaseAuth;

    @Inject
    NetworkChecker networkChecker;

    private ProgressDialog dialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ((KeringetanApp) getApplication()).getAppComponent().inject(this);
    }

    public FirebaseAuth getFirebaseAuth() {
        return firebaseAuth;
    }

    public NetworkChecker getNetworkChecker() {
        return networkChecker;
    }

    public void showProgressDialog() {
        dialog = new ProgressDialog(this);
        dialog.setMessage(getString(R.string.loading));
        if (dialog != null && !dialog.isShowing()) {
            dialog.show();
        }
    }

    public void dismissProgressDialog() {
        if (dialog != null) {
            dialog.dismiss();
        }
    }

    public void showSnackBar(View view, String message) {
        Snackbar snackbar = Snackbar.make(view, message, Snackbar.LENGTH_LONG);
        snackbar.getView().setBackgroundColor(ContextCompat.getColor(this, R.color.test));
        snackbar.show();
    }
}
