package com.indra.keringetan.module;

import com.indra.keringetan.KeringetanApp;
import com.indra.keringetan.base.BaseActivity;
import com.indra.keringetan.base.BaseFragment;
import com.indra.keringetan.base.LoginBaseActivity;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(modules = {
        AppModule.class,
        DataModule.class,
        NetworkModule.class,
        FirebaseAuthModule.class
})
public interface AppComponent {
    void inject(KeringetanApp keringetanApp);

    void inject(BaseActivity baseActivity);

    void inject(BaseFragment baseFragment);

    void inject(LoginBaseActivity loginBaseActivity);
}
