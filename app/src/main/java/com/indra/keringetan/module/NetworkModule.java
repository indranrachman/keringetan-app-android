package com.indra.keringetan.module;

import android.app.Application;

import com.indra.keringetan.utils.NetworkChecker;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class NetworkModule {
    @Provides
    @Singleton
    @SuppressWarnings("unused")
    NetworkChecker provideNetworkChecker(Application application) {
        return new NetworkChecker(application.getApplicationContext());
    }
}
