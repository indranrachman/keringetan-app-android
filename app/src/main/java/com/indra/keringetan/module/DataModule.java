package com.indra.keringetan.module;

import android.app.Application;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.indra.keringetan.AppConstant;
import com.indra.keringetan.service.UserPreference;
import com.indra.keringetan.service.UserService;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class DataModule {

    @Provides
    @Singleton
    @SuppressWarnings("unused")
    UserPreference provideUserPreference(Application application) {
        return UserPreference.getInstance(application.getApplicationContext());
    }

    @Provides
    @Singleton
    @SuppressWarnings("unused")
    UserService providesSpPreference(UserPreference preference) {
        return new UserService(preference);
    }

    @Provides
    @Singleton
    @SuppressWarnings("unused")
    DatabaseReference providesPostReference(FirebaseDatabase firebaseDatabase) {
        return firebaseDatabase.getReference().child(AppConstant.DatabaseReference.USER_POST);
    }

}