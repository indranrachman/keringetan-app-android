package com.indra.keringetan.model;

public class PostCategory {
    public int drawableResource;
    public String title;

    public PostCategory(int drawableResource, String title) {
        this.drawableResource = drawableResource;
        this.title = title;
    }
}
