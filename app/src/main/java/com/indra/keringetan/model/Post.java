package com.indra.keringetan.model;

public class Post {

    public String userName;
    public String description;
    public String location;
    public String dateTime;
    public String photoUrl;

    public Post() {
        /*
        used by firebase database to do serialiazed name
         */
    }

    public Post(String userName, String description, String location, String dateTime, String photoUrl) {
        this.userName = userName;
        this.description = description;
        this.location = location;
        this.dateTime = dateTime;
        this.photoUrl = photoUrl;
    }
}
