package com.indra.keringetan;

import android.app.Application;

import com.indra.keringetan.module.AppComponent;
import com.indra.keringetan.module.AppModule;
import com.indra.keringetan.module.DaggerAppComponent;
import com.indra.keringetan.module.DataModule;
import com.indra.keringetan.module.FirebaseAuthModule;
import com.indra.keringetan.module.NetworkModule;

public class KeringetanApp extends Application {
    private AppComponent appComponent;

    @Override
    public void onCreate() {
        super.onCreate();

        appComponent = DaggerAppComponent.builder().
                appModule(new AppModule(this)).
                firebaseAuthModule(new FirebaseAuthModule()).
                dataModule(new DataModule()).
                networkModule(new NetworkModule())
                .build();
        appComponent.inject(this);

    }

    public AppComponent getAppComponent() {
        return appComponent;
    }
}
