package com.indra.keringetan.main.explore;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;
import com.indra.keringetan.base.BasePresenter;
import com.indra.keringetan.model.Post;
import com.indra.keringetan.utils.NetworkChecker;

import java.util.List;

public class ExplorePresenter extends BasePresenter<ExploreView> implements ValueEventListener {

    private List<Post> posts;
    private DataSnapshot dataSnapshot;
    private boolean firstPage = true;

    public ExplorePresenter(ExploreView view, NetworkChecker networkChecker, List<Post> posts) {
        super(view, networkChecker);
        this.posts = posts;
    }

    public void onAddPostClicked() {
        view.goToPostActivity();
    }

    public void onNewPostAvailableClicked() {
        processPostData();
        view.removeNewPostNotification();
        view.presentPostData(posts);
    }

    public void addPostValueEventListener(DatabaseReference databaseReference) {
        databaseReference.orderByKey().addValueEventListener(this);
    }

    private void processPostData() {
        posts.clear();

        for (DataSnapshot noteSnapshot : dataSnapshot.getChildren()) {
            Post post = noteSnapshot.getValue(Post.class);
            posts.add(post);
        }
    }

    @Override
    public void onDataChange(DataSnapshot dataSnapshot) {
        this.dataSnapshot = dataSnapshot;
        view.showLoading();

        if (!firstPage) {
            view.removeLoading();
            view.createNewPostNotification();
            return;
        }

        processPostData();
        view.removeLoading();
        view.presentPostData(posts);
        firstPage = false;
    }

    @Override
    public void onCancelled(DatabaseError databaseError) {
        view.removeLoading();
        view.getUserPostFailed(databaseError.getMessage());
    }

    public void removeAddPostValueListener(DatabaseReference databaseReference) {
        databaseReference.removeEventListener(this);
    }
}
