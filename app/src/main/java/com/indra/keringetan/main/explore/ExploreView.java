package com.indra.keringetan.main.explore;

import com.indra.keringetan.base.BaseView;
import com.indra.keringetan.model.Post;

import java.util.List;

public interface ExploreView extends BaseView {
    void goToPostActivity();

    void presentPostData(List<Post> posts);

    void getUserPostFailed(String message);

    void createNewPostNotification();

    void removeNewPostNotification();
}
