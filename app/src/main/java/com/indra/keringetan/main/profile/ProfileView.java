package com.indra.keringetan.main.profile;

import com.indra.keringetan.base.BaseView;

public interface ProfileView extends BaseView {

    void signOutFromGoogleFailed(String statusMessage);

    void signOutFromGoogleSuccess();

    void connectToGoogleApiClient();

}
