package com.indra.keringetan.main.profile;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.indra.keringetan.R;
import com.indra.keringetan.base.BaseFragment;
import com.indra.keringetan.databinding.FragmentProfileBinding;
import com.indra.keringetan.login.LoginActivity;
import com.squareup.picasso.Picasso;

public class ProfileFragment extends BaseFragment implements ProfileView, GoogleApiClient.OnConnectionFailedListener, GoogleApiClient.ConnectionCallbacks {

    private FragmentProfileBinding binding;
    private ProfilePresenter presenter;
    private GoogleApiClient googleApiClient;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        presenter = new ProfilePresenter(this, getNetworkChecker(), getFirebaseAuth());
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        binding.textUserName.setText(getFirebaseUser().getDisplayName());
        Picasso.with(getContext()).load(getFirebaseUser().getPhotoUrl()).into(binding.profileImage);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_profile, container, false);
        binding.setPresenter(presenter);
        return binding.getRoot();
    }

    @Override
    public void showLoading() {
        showProgressDialog();
    }

    @Override
    public void removeLoading() {
        dismissProgressDialog();
    }

    @Override
    public void noInternetConnection() {
        showSnackBar(binding.getRoot(), getString(R.string.no_internet_connection));
    }

    @Override
    public void connectToGoogleApiClient() {
        GoogleSignInOptions signInOptions = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getString(R.string.default_web_client_id))
                .requestEmail()
                .build();

        googleApiClient = new GoogleApiClient.Builder(getContext())
                .enableAutoManage(getActivity(), this)
                .addConnectionCallbacks(this)
                .addApi(Auth.GOOGLE_SIGN_IN_API, signInOptions)
                .build();

        googleApiClient.connect();
    }

    @Override
    public void signOutFromGoogleFailed(String statusMessage) {
        showSnackBar(binding.getRoot(), statusMessage);
    }

    @Override
    public void signOutFromGoogleSuccess() {
        startActivity(new Intent(getActivity(), LoginActivity.class));
        getActivity().finish();
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        showSnackBar(binding.getRoot(), connectionResult.getErrorMessage());
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        presenter.signOutFromGoogle(googleApiClient);
    }

    @Override
    public void onConnectionSuspended(int i) {
    }
}
