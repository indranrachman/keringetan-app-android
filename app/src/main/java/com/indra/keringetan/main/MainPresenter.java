package com.indra.keringetan.main;

import android.support.annotation.IdRes;

import com.google.firebase.auth.FirebaseUser;
import com.indra.keringetan.R;
import com.indra.keringetan.utils.NetworkChecker;

public class MainPresenter {
    private MainView view;
    private NetworkChecker networkChecker;

    public MainPresenter(MainView view, NetworkChecker networkChecker) {
        this.view = view;
        this.networkChecker = networkChecker;
    }

    public void validateUserSession(FirebaseUser firebaseUser) {
        if (firebaseUser != null) {
            view.isLoggedOn();
        } else {
            view.isNotLoggedOn();
        }
    }

    public void getMenuScreen(@IdRes int tabId) {
        if (tabId == R.id.menu_explore) {
            view.showExploreMenu();
        } else if (tabId == R.id.menu_inbox) {
            view.showInboxMenu();
        } else if (tabId == R.id.menu_profile) {
            view.showProfileMenu();
        }
    }
}
