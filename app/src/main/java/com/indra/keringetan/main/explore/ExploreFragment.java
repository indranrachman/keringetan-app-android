package com.indra.keringetan.main.explore;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.indra.keringetan.AppConstant;
import com.indra.keringetan.R;
import com.indra.keringetan.base.BaseFragment;
import com.indra.keringetan.databinding.FragmentExploreBinding;
import com.indra.keringetan.model.Post;
import com.indra.keringetan.post.PostActivity;
import com.yqritc.recyclerviewflexibledivider.HorizontalDividerItemDecoration;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class ExploreFragment extends BaseFragment implements ExploreView, ExploreAdapter.OnPostClickedListener {

    private FragmentExploreBinding binding;
    private ExplorePresenter presenter;
    private LinearLayoutManager layoutManager;
    private ExploreAdapter adapter;
    private List<Post> posts = new ArrayList<>();

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        adapter = new ExploreAdapter(posts, getContext(), this);
        presenter = new ExplorePresenter(this, getNetworkChecker(), posts);
        layoutManager = new LinearLayoutManager(getContext());
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_explore, container, false);
        binding.setPresenter(presenter);

        binding.listPost.setHasFixedSize(true);
        binding.listPost.addItemDecoration(new HorizontalDividerItemDecoration.Builder(getContext()).showLastDivider().build());
        binding.listPost.setLayoutManager(layoutManager);
        binding.listPost.setAdapter(adapter);
        presenter.addPostValueEventListener(getPostDatabaseReference());

        return binding.getRoot();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        presenter.removeAddPostValueListener(getPostDatabaseReference());
    }

    @Override
    public void showLoading() {
        binding.progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void removeLoading() {
        binding.progressBar.setVisibility(View.GONE);
    }

    @Override
    public void noInternetConnection() {
        showSnackBar(binding.getRoot(), getString(R.string.no_internet_connection));
    }

    @Override
    public void goToPostActivity() {
        startActivity(new Intent(getContext(), PostActivity.class));
    }

    @Override
    public void presentPostData(List<Post> posts) {
        this.posts = posts;
        Collections.reverse(posts);
        adapter.notifyDataSetChanged();
    }

    @Override
    public void getUserPostFailed(String message) {
        showSnackBar(binding.getRoot(), message);
    }

    @Override
    public void createNewPostNotification() {
        binding.cardNewPostAvailable.setVisibility(View.VISIBLE);
        YoYo.with(Techniques.Shake).duration(AppConstant.ANIMATION_DURATION).playOn(binding.cardNewPostAvailable);
    }

    @Override
    public void removeNewPostNotification() {
        layoutManager.scrollToPosition(AppConstant.FIRST_PAGE);
        YoYo.with(Techniques.FadeOut).duration(AppConstant.ANIMATION_DURATION).playOn(binding.cardNewPostAvailable);
        binding.cardNewPostAvailable.setVisibility(View.VISIBLE);
    }

    @Override
    public void onPostItemClicked(int position) {
        showSnackBar(binding.getRoot(), posts.get(position).userName);
    }
}
