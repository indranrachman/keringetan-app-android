package com.indra.keringetan.main;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.IdRes;

import com.indra.keringetan.R;
import com.indra.keringetan.base.BaseActivity;
import com.indra.keringetan.databinding.ActivityMainBinding;
import com.indra.keringetan.login.LoginActivity;
import com.indra.keringetan.main.explore.ExploreFragment;
import com.indra.keringetan.main.inbox.InboxFragment;
import com.indra.keringetan.main.profile.ProfileFragment;
import com.roughike.bottombar.OnTabSelectListener;

public class MainActivity extends BaseActivity implements MainView, OnTabSelectListener {

    private ActivityMainBinding binding;
    private MainPresenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main);
        presenter = new MainPresenter(this, getNetworkChecker());
    }

    @Override
    protected void onStart() {
        super.onStart();
        presenter.validateUserSession(getFirebaseAuth().getCurrentUser());
    }

    @Override
    public void isLoggedOn() {
        binding.bottomBar.setOnTabSelectListener(this);
    }

    @Override
    public void isNotLoggedOn() {
        startActivity(new Intent(MainActivity.this, LoginActivity.class));
        finish();
    }

    @Override
    public void showExploreMenu() {
        ExploreFragment fragment = new ExploreFragment();
        String tag = InboxFragment.class.getSimpleName();
        changeFragment(fragment, tag);
    }

    @Override
    public void showInboxMenu() {
        InboxFragment fragment = new InboxFragment();
        String tag = InboxFragment.class.getSimpleName();
        changeFragment(fragment, tag);
    }

    @Override
    public void showProfileMenu() {
        ProfileFragment fragment = new ProfileFragment();
        String tag = ProfileFragment.class.getSimpleName();
        changeFragment(fragment, tag);
    }

    @Override
    public void onTabSelected(@IdRes int tabId) {
        presenter.getMenuScreen(tabId);
    }
}
