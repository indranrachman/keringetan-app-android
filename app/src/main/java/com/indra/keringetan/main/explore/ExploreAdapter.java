package com.indra.keringetan.main.explore;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.indra.keringetan.R;
import com.indra.keringetan.databinding.ItemPostBinding;
import com.indra.keringetan.model.Post;
import com.squareup.picasso.Picasso;

import java.util.List;

public class ExploreAdapter extends RecyclerView.Adapter<ExploreAdapter.ExploreViewHolder> {
    private List<Post> postList;
    private OnPostClickedListener listener;
    private Context context;

    interface OnPostClickedListener {
        void onPostItemClicked(int position);
    }

    class ExploreViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public ItemPostBinding binding;
        public OnPostClickedListener listener;

        private ExploreViewHolder(ItemPostBinding binding, OnPostClickedListener listener) {
            super(binding.getRoot());
            this.listener = listener;
            this.binding = binding;
            this.binding.getRoot().setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            listener.onPostItemClicked(getAdapterPosition());
        }
    }

    ExploreAdapter(List<Post> postList, Context context, OnPostClickedListener listener) {
        this.context = context;
        this.postList = postList;
        this.listener = listener;
    }

    @Override
    public void onBindViewHolder(ExploreViewHolder holder, int position) {
        Post post = postList.get(position);
        Picasso.with(context).load(post.photoUrl).into(holder.binding.imageDisplayPicture);
        holder.binding.textDisplayName.setText(post.userName);
        holder.binding.textLocation.setText(post.location);
        holder.binding.textDescription.setText(post.description);

    }

    @Override
    public int getItemCount() {
        return postList.size();
    }

    @Override
    public ExploreViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        ItemPostBinding binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.item_post, parent, false);
        return new ExploreViewHolder(binding, listener);
    }
}
