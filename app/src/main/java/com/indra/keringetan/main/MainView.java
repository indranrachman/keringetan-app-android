package com.indra.keringetan.main;

public interface MainView {
    void isLoggedOn();

    void isNotLoggedOn();

    void showExploreMenu();

    void showInboxMenu();

    void showProfileMenu();
}
