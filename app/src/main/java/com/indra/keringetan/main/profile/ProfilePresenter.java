package com.indra.keringetan.main.profile;

import android.support.annotation.NonNull;

import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.firebase.auth.FirebaseAuth;
import com.indra.keringetan.base.BasePresenter;
import com.indra.keringetan.utils.NetworkChecker;

public class ProfilePresenter extends BasePresenter<ProfileView> {

    private FirebaseAuth firebaseAuth;

    public ProfilePresenter(ProfileView view, NetworkChecker networkChecker, FirebaseAuth firebaseAuth) {
        super(view, networkChecker);
        this.firebaseAuth = firebaseAuth;
    }

    public void onSignOutClicked() {
        view.showLoading();
        view.connectToGoogleApiClient();
    }

    public void signOutFromGoogle(GoogleApiClient googleApiClient) {
        firebaseAuth.signOut();
        Auth.GoogleSignInApi.signOut(googleApiClient).setResultCallback(
                new ResultCallback<Status>() {
                    @Override
                    public void onResult(@NonNull Status status) {
                        if (status.isSuccess()) {
                            view.removeLoading();
                            view.signOutFromGoogleSuccess();
                            return;
                        }
                        view.removeLoading();
                        view.signOutFromGoogleFailed(status.getStatusMessage());
                    }
                });
    }
}
