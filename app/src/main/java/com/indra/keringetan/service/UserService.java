package com.indra.keringetan.service;

public class UserService {
    private static final String USER_ID = "user_id";
    private static final String USER_NAME = "user_name";
    private static final String USER_EMAIL = "user_email";
    private static final String USER_PHONE = "user_phone";
    private static final String USER_PHOTO_URL = "user_photo_url";
    private static final String ACCESS_TOKEN = "access_token";
    private static final String REFRESH_TOKEN = "refreshtoken";
    private static final String LATITUDE = "latitude";
    private static final String LONGITUDE = "longitude";

    private final UserPreference preference;

    public UserService(UserPreference preference) {
        this.preference = preference;
    }

    public String getUserPhotoUrl() {
        return preference.getString(USER_PHOTO_URL, "");
    }

    public String getUserName() {
        return preference.getString(USER_NAME, "");
    }

    public String getUserEmail() {
        return preference.getString(USER_EMAIL, "");
    }

    public String getUserPhone() {
        return preference.getString(USER_PHONE, null);
    }

    public void setUserName(String userName) {
        preference.setString(USER_NAME, userName);
    }

    public void setUserEmail(String userEmail) {
        preference.setString(USER_EMAIL, userEmail);
    }

    public void setUserPhone(String userPhone) {
        preference.setString(USER_PHONE, userPhone);
    }

    public String getRefreshToken() {
        return preference.getString(REFRESH_TOKEN, "");
    }

    public String getAccessToken() {
        return preference.getString(ACCESS_TOKEN, "");
    }

    public int getUserId() {
        return preference.getInt(USER_ID, 0);
    }

    public String getLatitude() {
        return preference.getString(LATITUDE, null);
    }

    public String getLongitude() {
        return preference.getString(LONGITUDE, null);
    }

    public void setAccessToken(String token) {
        preference.setString(ACCESS_TOKEN, token);
    }

    public void setRefreshToken(String refreshToken) {
        preference.setString(REFRESH_TOKEN, refreshToken);
    }

    public void setUserId(int userId) {
        preference.setInt(USER_ID, userId);
    }

    public void setLatitude(String latitude) {
        preference.setString(LATITUDE, latitude);
    }

    public void setLongitude(String longitude) {
        preference.setString(LONGITUDE, longitude);
    }

    public void setUserPhotoUrl(String userPhotoUrl) {
        preference.setString(USER_PHOTO_URL, userPhotoUrl);
    }

    public void clearUser() {
        preference.remove(USER_ID);
        preference.remove(LONGITUDE);
        preference.remove(LATITUDE);
        preference.remove(ACCESS_TOKEN);
        preference.remove(REFRESH_TOKEN);
    }
}
