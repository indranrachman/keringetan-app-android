package com.indra.keringetan;

public class AppConstant {

    public static final int FIRST_PAGE = 0;
    public static final int ANIMATION_DURATION = 700;
    public static final int LIMIT_PER_PAGE = 10;
    public static final int LOCATION_PICKER_REQUEST = 1;

    public static class DatabaseReference {
        public static String USER_POST = "posts";

        public static class ChildReference {
            public static final String DATE_TIME = "dateTime";
            public static final String USER_NAME = "userName";
            public static final String LOCATION = "userName";
            public static final String DESCRIPTION = "description";
        }
    }

    public static class SportCategory {
        public static int CYCLING = 1;
        public static int RUNNING = 2;
        public static int BASKET = 3;
        public static int SOCCER = 4;
        public static int FUTSAL = 5;
        public static int YOGA = 6;
        public static int ZUMBA = 7;
    }
}
